package service

import (
	"context"

	"gitlab.com/proemergotech-public/project-skeleton-go/app/schema"
	"gitlab.com/proemergotech-public/project-skeleton-go/app/storage"
)

type Service struct {
	redis *storage.Redis
}

func NewService(
	redis *storage.Redis,
) *Service {
	return &Service{
		redis: redis,
	}
}

func (s *Service) Dummy(ctx context.Context) (*schema.Dummy, error) {
	dummy, err := s.redis.GetDummy(ctx)
	if err != nil {
		return nil, err
	}

	return dummy, nil
}
